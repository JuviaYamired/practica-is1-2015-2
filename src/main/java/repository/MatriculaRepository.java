package repository;

import java.util.Collection;

import domain.Matricula;

public interface MatriculaRepository extends BaseRepository<Matricula, Long> {
	
	Matricula findById(String Id);
	
}
