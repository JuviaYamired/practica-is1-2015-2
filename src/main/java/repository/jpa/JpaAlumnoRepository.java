package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.AlumnoRepository;
import domain.Alumno;

@Repository
public class JpaAlumnoRepository extends JpaBaseRepository<Alumno, Long> implements
	AlumnoRepository {

	@Override
	public Alumno findById(String id) {
		String jpaQuery = "SELECT a FROM Alumno a WHERE a.id = :id";
		TypedQuery<Alumno> query = entityManager.createQuery(jpaQuery, Alumno.class);
		query.setParameter("id", id);
		return getFirstResult(query);
	}
	
	@Override
	public Alumno findByApellidoPaterno(String apellido) {
		String jpaQuery = "SELECT a FROM Alumno a WHERE a.apellidoPaterno = :apellido";
		TypedQuery<Alumno> query = entityManager.createQuery(jpaQuery, Alumno.class);
		query.setParameter("apellido", apellido);
		return getFirstResult(query);
	}
}
