package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.MatriculaRepository;
import domain.Matricula;

@Repository
public class JpaMatriculaRepository extends JpaBaseRepository<Matricula, Long> implements
	MatriculaRepository {

	@Override
	public Matricula findById(String id) {
		String jpaQuery = "SELECT a FROM Matricula a WHERE a.id = :id";
		TypedQuery<Matricula> query = entityManager.createQuery(jpaQuery, Matricula.class);
		query.setParameter("id", id);
		return getFirstResult(query);
	}
}
