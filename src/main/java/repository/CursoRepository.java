package repository;

import java.util.Collection;
import domain.Curso;

public interface CursoRepository extends BaseRepository<Curso, Long> {
	
	Curso findByNombre(String nombre);
	Curso findByCodigo(String codigo);
	Collection<Curso> getAll();
}
