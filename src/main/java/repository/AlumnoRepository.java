package repository;

import java.util.Collection;

import domain.Alumno;

public interface AlumnoRepository extends BaseRepository<Alumno, Long> {
	
	Alumno findById(String id);
	Alumno findByApellidoPaterno(String apellido);
}
